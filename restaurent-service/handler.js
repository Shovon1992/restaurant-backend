'use strict';
const dbService = require('./database/restaurentDbService');

module.exports.hello = async event => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v1.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

module.exports.Resturant =  (event, context, callback) => 
{
  console.log('request data ',event);
  //const queryStringParameters =  event.queryStringParameters;
 // triggerSource: 'PreSignUp_AdminCreateUser',
 if(event.triggerSource != null)
 {
  event.request.userAttributes.authId = event.userName;
  const data = event.request.userAttributes;

  dbService.restaurantDbService.createNewRestaurant(data, (err, res)=>{
    context.succeed(event);
  });
 }else{
  const requestMethod = event.httpMethod;
  
  switch(requestMethod){
    case 'GET' :
      const pathParameter =  event.pathParameters;
      if(pathParameter.authId != null){
        dbService.restaurantDbService.getResturantByAuthID(pathParameter, callback);
      }else if (pathParameter.id != null){
        dbService.restaurantDbService.getResturantDetails(pathParameter, callback);
      }else{
        dbService.restaurantDbService.getResturant(null, callback);
      }
      break;
    case 'POST' :
      //dbService.restaurantDbService.createNewRestaurant(event.body,callback);
      break;
    case 'PUT' :
      dbService.restaurantDbService.updateRestaurant(event.body, callback);
      break;
    // default :
    //   dbService.restaurantDbService.getResturant(null, callback);
    //   break;
  };
  
 }
 
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

 /**
   * Restaurant welcome screen operation 
   */
  
  module.exports.RestaurantWelcomeScreen = (event, context, callback) => {
    switch(event.httpMethod) {
      case 'GET' : 
        dbService.restaurantDbService.getWelcomeScreen(event.pathParameters,callback);
        break;
      case 'POST' : 
        dbService.restaurantDbService.creteWelcomeScreen(event.body, callback);
        break;
    }
  }

  module.exports.ResturantDiningTable =  (event, context, callback) => 
{
  console.log('request data ',event.body);
  //const queryStringParameters =  event.queryStringParameters;
 
  const requestMethod = event.httpMethod;
  dbService.restaurantDbService.insertTable(event.body, callback);
  
  switch(requestMethod){
    case 'GET' :
      dbService.restaurantDbService.getAllTable(event.pathParameters, callback);
      break;
    case 'POST' :
      dbService.restaurantDbService.insertTable(event.body, callback);
      break;
    case 'PUT' :
      dbService.restaurantDbService.updateTableInfo(event.body, callback);
      break;

    case 'PATCH' :
      dbService.restaurantDbService.deleteTableinfo(event.body, callback);
      break;
    // default :
    //   dbService.restaurantDbService.getResturant(null, callback);
    //   break;
  };
  

 
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
