
/** restaurant modules*/
module.exports.getResturants = require('./Query/Resturant/getRrstaurant');
module.exports.getResturantDetails = require('./Query/Resturant/getResturantDetails');
module.exports.getResturantDetailsByAuthId = require('./Query/Resturant/getResturantDetailsByAuthId');

module.exports.addNewrestaurant = require('./Query/Resturant/addResturant');
module.exports.updateRestaurant = require('./Query/Resturant/update');
module.exports.addRestaurantQrcode = require('./Query/Resturant/add_qrcode');

/**  restaurant welcome screen module */

module.exports.creteWelcomeScreen = require('./Query/RestauranWelcome/createNewWlcomeScreen');
module.exports.getWelcomeScreen = require('./Query/RestauranWelcome/getWelcomeScreens');


/**  restaurant welcome screen module */

module.exports.diningTable = require('./Query/dining_table/dining_table');
module.exports.addTableQrcode = require('./Query/dining_table/add_qrcode');
