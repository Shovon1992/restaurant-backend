let queries = require('./statement');
var excute = require('../mariaDbCon/exe');
var QRCode = require('qrcode');
const AWS = require('aws-sdk');

let s3bucket = new AWS.S3({
    accessKeyId: "AKIAT4BDBURKVH7G3DXN",
    secretAccessKey: "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",
    region: "us-east-1"
  });

module.exports = {

    /** get all resturant list  */
    getResturant: function(data, callback){
        var sql = queries.getResturants;
        excute.execute(sql, data, callback);
    },
    getResturantByAuthID: function(data, callback){
        var sql = queries.getResturantDetailsByAuthId;
        excute.execute(sql, data, callback);
    },


    /**Get particular resturant details  */
    getResturantDetails : function( data, callback){
        var sql  = queries.getResturantDetails;
        excute.execute(sql,{'id': data.id}, callback);
    },

    /** Add new restaurant  */
    createNewRestaurant : function(data,callback) {
        /* data = JSON.parse(data)
        console.log('dddd',data)
        var createData = {
            id : null,
            tagLine :  data.tagLine,
            reg_no : data.reg_no,
            data:JSON.stringify(data),
            name :  data.name,
            email : data.email,
            phone : data.phone,
            image : (data.image) ? data.image : 'https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
            accountId:data.accountId, 
        }
        console.log(createData); */
        data.image = (data.image ? data.image : 
            'https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80');
          
        var sql = queries.addNewrestaurant;
    excute.execute(sql,{data:JSON.stringify(data)},(err, res)=>{
        let bdata = JSON.parse(res.body).data;
        console.log('data-----------', bdata[3][0]);
        data.id = bdata[3][0].id;
        data.key = "image/qr/" + data.id +'/'+ data.name + Date.now().toString() + '_qr_code.png'
        genarateQrCode(data, (err, res)=>{
             var sql  = queries.addRestaurantQrcode;
             excute.execute(sql,res, callback);
        });
        callback(err, res);
    });
       
    } ,

    /** update old restaurant  */
    updateRestaurant : function(data,callback) {
        data = JSON.parse(data)
        console.log('dddd',data)
        var createData = {
            
            tagLine :  data.tagLine,
            data:JSON.stringify(data),
            name :  data.name,
            email : data.email,
            phone : data.phone,
            image : (data.image) ? data.image : 'https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
            accountId:data.accountId, 
            id: data.id
        }
        console.log(createData);
        var sql = queries.updateRestaurant;
        excute.execute(sql,createData,callback);
    } ,

    /**
     * Create restaurant welcome screen data
     */
    creteWelcomeScreen : function(data,callback){
        data = JSON.parse(data)
        let createData  ={
            id : null,
            header: data.header,
            resturant_hash :  data.resturant_hash,
            description : data.description,
            image : (data.image) ? data.image : 'https://fubellyhouston.com/wp-content/uploads/2016/11/Job-Opening-for-Chef_1.png'
        }
        let sql = queries.creteWelcomeScreen;
        console.log(sql)
        excute.execute(sql,createData,callback);
    },
    /**
     * Get Welcome screen lists 
     */

    getWelcomeScreen : function( data, callback){
        var sql  = queries.getWelcomeScreen;
        excute.execute(sql,{resturantId: data.resturantId}, callback);
    },

    //-------------------dining table------------------------------
    insertTable : function( data, callback){
        var sql  = queries.diningTable;
        excute.execute(sql,{task: 'insert', idata:data}, (err,res)=>{
            let bdata = JSON.parse(res.body).data;
            console.log('data-----------', bdata[0][0]);
        //console.log('data-----------', bdata[2][0]);
        data = JSON.parse(data);
        data.id = bdata[0][0].id;
        data.restaurantName = bdata[0][0].restuName
        data.key = "image/menume/qr/"+data.restaurantId+'/'+data.restaurantName +'/'+ + data.id +'/'+ data.id + Date.now().toString() + '_qr_code.png'
        genarateQrCode(data, (err, res)=>{
            console.log('genarateQrCode callback:', res);
            var sql  = queries.addTableQrcode;
             excute.execute(sql,res, callback);
        });
        callback(err, res);
        });
    },

    updateTableInfo : function( data, callback){
        var sql  = queries.diningTable;
        excute.execute(sql,{task: 'update', idata: JSON.stringify(data)}, callback);
    },
    deleteTableinfo : function( data, callback){
        var sql  = queries.diningTable;
        excute.execute(sql,{task: 'delete', idata: JSON.stringify(data)}, callback);
    },

    getAllTable : function( data, callback){
        var sql  = queries.diningTable;
        excute.execute(sql,{task: 'get', idata: JSON.stringify(data)}, callback);
    },
} 

function genarateQrCode(data, callback)
{
    
    var opts = {
        errorCorrectionLevel: 'H',
        type: 'image/jpeg',
        quality: 0.5,
        margin: 1,
        width:120,
        color: {
          dark:"#010599FF",
          light:"#FFBF60FF"
        }
      }

      var segs = [
        { id: data.id, mode: 'alphanumeric' },
        { restaurantName: data.restaurantName, mode: 'alphanumeric' },
        { restaurantId: data.restaurantId, mode: 'alphanumeric' },
        {quality: 0.5,mode: 'numeric'},
        {margin: 1 , mode: 'numeric'},
        {width:120, mode: 'numeric'},
        { errorCorrectionLevel: 'H',mode: 'alphanumeric'},
        {createdAt: Date.now().toString(),mode: 'alphanumeric'}
        
      ]
       
    QRCode.create("restaurant", segs, opts );
    QRCode.toBuffer('restaurant', segs, function (err, buffer) {
        if (err) throw err
       
       console.log(buffer);
       data.file = buffer;
        console.log('-------before upload call:',data);
        upload(data, callback);
      });

    /* QRCode.toString('restaurant', segs,function (err, data) {
        console.log(data);
    }); */
   
}

function upload(data, callback)
{
    const params = {
        Bucket: "menume-dev-restaurant-doc",
        Key: data.key,
        Body: data.file,
        ContentType: "image/png",
        ACL: 'public-read'
      };

      console.log(params)

      s3bucket.upload(params, async (err, udata) => {
        try {
          if (err) {
            //res.status(500).json({ error: true, Message: err });
            console.log(err);
           } else {
            // console.log(data);
             data.loc = udata.Location;
             //--------------save in db
            // var sql  = queries.addRestaurantQrcode;
             //excute.execute(sql,data, callback);
             callback(err, data);
         }
        } catch (err) {
            console.log(err);
        }
      });
}