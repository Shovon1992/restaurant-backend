let udQueries = require('./quaries/statement');
var excute = require('../mariaDbCon/exe');

module.exports = {
    createUserInfo: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.userInfo;
       excute.execute(sql,{'task': 'insert','data': JSON.stringify(data)} , callBack);
        
    },

    updateUserInfo: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.userInfo;
       excute.execute(sql,{'task': 'update','data': data} , callBack);
        
    },

    getUserById: function(id, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.userInfo;
       excute.execute(sql,{'task': 'getById','data': JSON.stringify({'id':id})} , callBack);
        
    },
    getUsers: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.userInfo;
       excute.execute(sql,{'task': 'default', 'data': null} , callBack);
        
    },
    getByCognitoId: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.userInfo;
       excute.execute(sql,{'task': 'getByAuthId','data': data} , callBack);
        
    },
} 