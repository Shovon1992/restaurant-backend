const {createPool}      = require('mysql');

exports.establishConnection = function() {
  const pool = createPool({
    host     : 'resturant-dev.c0eahyostljd.us-east-1.rds.amazonaws.com',
    user     : 'admin',
    password : 'admin_123',
    database : 'resturant_dev',
    connectionLimit:10,
    multipleStatements:true

  });

  pool.config.connectionConfig.queryFormat = function (query, values) {
    if (!values) return query;
    return query.replace(/\:(\w+)/g, function (txt, key) {
      if (values.hasOwnProperty(key)) {
        return this.escape(values[key]);
      }
      return txt;
    }.bind(this));
  };

  return pool;
}