'use strict';
const dbService = require('./database/cgsUserServiceDbService');

module.exports.hello = async event => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v1.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
module.exports.userInfo = (event, context, callback) => {
  console.log(event);
  console.log(event.userName);
  const data = {'authId': event.userName, 
                'phone': event.request.userAttributes.phone_number,
              'email':event.request.userAttributes.email,
              dob:event.request.userAttributes.birthdate,
              gender:event.request.userAttributes.gender,
              name:event.request.userAttributes.name};
  dbService.userDbService.createUserInfo(data, (err, res)=>{
    context.succeed(event);
  });
}

module.exports.userData = (event, context, callback) => {
  switch(event.httpMethod) {
    case 'PUT' : 
      dbService.userDbService.updateUserInfo(event.pathParameters,callback);
      break;
    case 'GET' : 
    const pathParameter =  event.pathParameters;
    if(pathParameter){
      dbService.userDbService.getUserById(pathParameter.id, callback);
    }else{
      dbService.userDbService.getUsers(null, callback);
    }
      break;

    case 'POST' : 
    dbService.userDbService.getByCognitoId(event.body, callback);
    break;
  }
}