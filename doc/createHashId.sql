CREATE DEFINER=`admin`@`%` FUNCTION `createHashId`(tableName text
) RETURNS text CHARSET latin1
BEGIN
	declare tempId varchar(100);
	set @lastId = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = tableName );
    return ( SELECT SHA1(CONCAT(now(), rand(),@lastId, tableName )) );
RETURN 1;
END