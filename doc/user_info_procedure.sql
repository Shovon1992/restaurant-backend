CREATE DEFINER=`admin`@`%` PROCEDURE `user_info_procedure`(
				in task text(120), /* select insert update */
                in idata json)
BEGIN
		
		set @first_name = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.firstName'));
		set @last_name = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.lastName'));
		set @email = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.email'));
		set @phone = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.phone'));
        set @gender = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.gender'));
        set @dob = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.dob'));
        set @name = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.name'));
        
        case task 
			when 'insert' then
            set @authId = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.authId'));
            set @uid = (SELECT AUTO_INCREMENT 
					FROM information_schema.TABLES WHERE TABLE_NAME = "user_info");
                    
			set @hash_id = (SELECT SHA1(CONCAT(now(),@uId,'user_info')));
          
            
			INSERT INTO user_info
						(gender,
                        dob,
                        name,
						phone,
						email,
						createdBy,
						createdOn,
						hash_id)
						VALUES
						(@gender,
                        @dob,
                        @name,                        
						@phone,
						@email,
						1,
						now(),
						@hash_id);
                        
			INSERT INTO user_cognito_map
					(
					user_id,
					cognito_id,
					createdBy,
					createdOn)
					VALUES
					(
					@uid,
					@authId,
					1,
					now());
                    
		INSERT into user_profile_image (user_info_id, createdOn) values(@uid, now());
                    
			when 'update' then 
					set @uid =(select id from user_info 
                    where hash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.id')));
                    
				if @first_name is not null then
					UPDATE user_info
					SET
					gender = If(@gender is null, gender, @gender),
					dob = If(@dob is null, dob, @dob),
					name = If(@name is null, name, @name),
					first_name = @first_name,
					last_name = @last_name,
					modifiedBy = @uid ,
					modifiedOn = now()

					WHERE id = @uid and id>0;
                    
                    end if;
                    
		set @image_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.imageUrl'));
		set @image_thumb_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.thumbUrl'));
				if @image_url is not null then
                update user_profile_image 
					set image_url = @image_url,
                    image_thumb_url = @image_thumb_url
                    
                    where user_info_id = @uid and id>0;
                 end if;
                
			when 'getById' then
            	SELECT 
				ui.first_name as firstName,
				ui.last_name as lastName,
                ui.gender,
				ui.dob,
                ui.name,
				ui.phone as phone,
				ui.email as email,
                ui.hash_id as id,
                upimg.image_url as imageUrl,
                upimg.image_thumb_url as thumbUrl
			
		FROM user_info ui 
        left join user_profile_image upimg on ui.id = upimg.user_info_id
        where  ui.hash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.id'));
            
		when 'getByAuthId' then             
				SELECT 
				ui.first_name as firstName,
				ui.last_name as lastName,
				ui.phone as phone,
				ui.email as email,
                ui.hash_id as id,
                upimg.image_url as imageUrl,
                upimg.image_thumb_url as thumbUrl 
			
		FROM user_info ui
        left join user_cognito_map umap on ui.id = umap.user_id
        left join user_profile_image upimg on ui.id = upimg.user_info_id
        where   umap.cognito_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.authId'));
        
		else
		SELECT 
				ui.first_name as fisrstName,
				ui.last_name as lastName,
				ui.phone as phone,
				ui.email as email,
                ui.hash_id as id,
                upimg.image_url as imageUrl,
                upimg.image_thumb_url as thumbUrl 
			
		FROM user_info ui
        left join user_profile_image upimg on ui.id = upimg.user_info_id
        ;
        
        end case;
END