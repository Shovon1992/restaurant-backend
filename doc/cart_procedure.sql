CREATE DEFINER=`admin`@`%` PROCEDURE `cart_procedure`(in task text(20), in idata json)
BEGIN
	declare ihash_id text(255) default null;
    
    case task 
		when 'createCart' then -- For insert data in cart
			 -- for user id
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.userId') );
			set @userId = (select id from user_info where hash_id = ihash_id);
			-- for restauratant 
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.restaurantId') );
			set @restaurantId = ( select id from restaurant where hash_id =  ihash_id);
			
			set @tableId = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.tableId'));
            -- set @tableId = ( select id from dining_table where hash_id = (JSON_UNQUOTE(JSON_EXTRACT(idata,'$.tableId'))) );
			set @lastId = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = "cart");
			set @cartHash = (SELECT SHA1(CONCAT(now(), rand(),@lastId,'cart')));
             -- for food item id
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.foodItemId') );
			set @foodItemId = ( select id from food_menu_item where hash_id =  ihash_id);
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.cartId') );
			set @cartId =  ( select id from cart where hash_id =  ihash_id);
			
			-- for food item price 
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.priceId') );
			set @priceId =  ( select id from food_menu_item_price where hash_id =  ihash_id);
			
			set @addedQuantity = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.addedQuantity'));
            set @ifCartAvailable = (select id from cart where userId = @userId and restaurantId= @restaurantId and tableId = @tableId and  id >0 ); # check if user already created a crt and 
            
            case @ifCartAvailable
				when @ifCartAvailable then
					
                    
                    set @ifItemAvailble = ( select id from cart_items where cartId = @ifCartAvailable and foodItemId = @foodItemId);
                    set @qty = @addedQuantity + (select addedQuantity from cart_items where id = @ifItemAvailble);
                    
                    if @ifItemAvailble then
                    select @ifItemAvailble, @qty;
						if (@qty > 0) = 1 then
							update cart_items set addedQuantity = @qty, status = 1 where  id = @ifItemAvailble; -- delete food item from cart
						else 
                        update cart_items set status = -1, addedQuantity = 0 where id = @ifItemAvailble and id > 0; -- delete food item from cart
							#select @qty = 0;
							
						end if;
					else 
						insert into cart_items (
							foodItemId,
							priceId,
							addedQuantity,
							cartId,
							hash_id
						) values (
							@foodItemId,
							@priceId,
							@addedQuantity, 
							@ifCartAvailable,
							createHashId('cart_items')
						);
					end if;
                else 
					insert into cart ( userId, restaurantId, tableId, createdBy, hash_id ) values (
						@userId, @restaurantId, @tableId, @userId, @cartHash );
					set @lastId = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = 'cart' );
                    insert into cart_items (
						foodItemId,
						priceId,
						addedQuantity,
						cartId,
						hash_id
					) values (
						@foodItemId,
						@priceId,
						@addedQuantity, 
						@lastId,
						createHashId('cart_items')
					);
                    
			end case;
            
        when 'insertfoodItem' then -- for inserting food item
			 -- for food item id
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.foodItemId') );
			set @foodItemId = ( select id from food_menu_item where hash_id =  ihash_id);
			-- for cart id 
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.cartId') );
			set @cartId =  ( select id from cart where hash_id =  ihash_id);
            
            -- for food item price 
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.priceId') );
			set @priceId =  ( select id from food_menu_item_price where hash_id =  ihash_id);
			
            set @addedQuantity = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.addedQuantity'));
            -- set @lastId = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = "cart_items");
			-- set @cartHash = (SELECT SHA1(CONCAT(now(), rand(),@lastId,'cart_items')));
            insert into cart_items (
				foodItemId,
                priceId,
                addedQuantity,
                cartId,
                hash_id
            ) values (
				@foodItemId,
				@priceId,
				@addedQuantity, 
                @cartId,
				createHashId('cart_items')
            );
            
		when 'updateCartItemQty' then -- delete items from cart    
        
            -- get cart id 
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.cartItemId') );
			set @cartItemId = ( select id from cart_items where hash_id =  ihash_id);
            
            set @qty =   JSON_EXTRACT(idata,'$.addedQuantity') ;

            -- update cart_items set addedQuantity = @qty where  id = @cartItemId; -- delete food item from cart
           case @qty 
			 when (@qty < 1) = 0 then
				update cart_items set status = -1, addedQuantity = 0 where id = @cartItemId and id > 0; -- delete food item from cart
			 else 
				#select @qty = 0;
				 update cart_items set addedQuantity = @qty where  id = @cartItemId; -- delete food item from cart
             end case;
            
        when 'deleteFromCart' then -- delete items from cart
			
        -- get cart id 
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.cartId') );
			set @cartId = ( select id from food_menu_item where hash_id =  ihash_id);
            
		 -- get cart id 
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.foodItemId') );
			set @foodItemId = ( select id from food_menu_item where hash_id =  ihash_id);
           
           set @tableId = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.tableId'));
		-- Get total numbers of cart items 
            set @totalCartItems = (select count * form cart_items where cartId = @cartId and status = 1 );
            
		-- if there is only one items in cart 
			case @totalCartItems 
				when @totalCartItems >1 then 
					update cart_items set status = -1 where id = @foodItemId; -- delete food item from cart
                else
					update cart set status = -1 where id = @cartId; -- make cart un available 
                    update cart_items set status = -1 where id = @foodItemId; -- delete food item from cart
			end case;
		-- Get store details 
        when 'getCartDetails' then
	  -- get cart id 
	set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.userId') );
	set @userId = ( select id from user_info where hash_id =  ihash_id);
            -- get cart id 
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.restaurantId') );
			set @restaurantId = ( select id from restaurant where hash_id =  ihash_id);
            set @tableId = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.tableId'));
            -- set @ cartId = (select id from cart where restaurantId = @restaurantId and userId = @userId ); 
             
             select 
		c.hash_id as cartId,
                ci.addedQuantity,
                ci.hash_id as cartItemId,
                fmi.name,
                fmi.description,
                fmi.hash_id as foodItemId,
                fmPrice.quantity,
		fmPrice.price,
		fmPrice.currency,
		fmPrice.unit,
                fmPrice.hash_id as priceId,
                fimage.image_url as foodLargeImageUrl,
		fimage.image_thumb_url as foodThumbUrl
                
		from cart c
		left join cart_items ci on ci.cartId = c.id 
		left join food_menu_item fmi on fmi.id = ci.foodItemId 
		left join food_menu_item_image fimage on fimage.food_menu_item_id = fmi.id and fimage.set_as_logo = 1 
		left join food_menu_item_price fmPrice on fmPrice.id = ci.priceId 
		
               where  c.tableId = @tableId and ci.status = 1 and c.userId = @userId and c.restaurantId = @restaurantId ;
               
		else 
			select * from cart;
           
        end case;
END