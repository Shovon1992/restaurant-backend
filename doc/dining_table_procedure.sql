CREATE DEFINER=`admin`@`%` PROCEDURE `dining_table_procedure`(in task text(120), in idata json)
BEGIN
			DECLARE ihash_id TEXT(426) DEFAULT null;
			declare countFrom int default 0;
            declare count int default 20;
			
			set ihash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.id'));
			
			case task	
			
			when 'insert' then 
            
            	set @image_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.imageUrl'));
		set @thumb_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.thumbUrl'));
		set @description = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.description'));
		set @table_code = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.tableCode'));
		set @table_type = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.tableType'));
		
		set @seat_count = JSON_EXTRACT(idata,'$.seatCount');
		set @availability = JSON_EXTRACT(idata,'$.availability');
		
		set ihash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.restaurantId'));
		
		set @itemId = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = "dininng_table");
		set @itemHId = (SELECT SHA1(CONCAT(now(), rand(),@itemId,'dininng_table')));
                 
		INSERT INTO dininng_table
				(
				restaurent_id,
				table_code,
				description,
				seat_count,
				table_type,
				availability,
				hash_id,
				status)
				VALUES
				(
				(Select id from restaurant where hash_id = ihash_id),
				@table_code,
				@description,
				@seat_count,
				@table_type,
				@availability,
				@itemHId);
			
	when 'update' then 
		set @image_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.imageUrl'));
		set @thumb_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.thumbUrl'));
		
		set @description = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.description'));
		set @table_code = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.tableCode'));
		set @table_type = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.tableType'));
		
		set @seat_count = JSON_EXTRACT(idata,'$.seatCount');
		set @seat_count = JSON_EXTRACT(idata,'$.seatCount');
		set @occupied = JSON_EXTRACT(idata,'$.occupied');
		
				
		UPDATE dininng_table
		SET

		table_code = If(@table_code is null, table_code , @table_code),
		description = If(@description is null, description , @description),
		seat_count = If(@seat_count is null, seat_count , @seat_count),
		table_type = If(@table_type is null, table_type , @table_type),
		availability = If(@availability is null, availability , @availability),
		occupied = if(@occupied is null, occupied , @occupied),
		modifiedOn = now() 

		where hash_id = ihash_id and id > 0;
	
	when 'delete' then
	
		update dininng_table  
			set status = -1 
		where hash_id = ihash_id and id > 0;
	
	when 'get_by_id' then 
	
	SELECT    
	    dt.table_code as tableCode,
	    dt.description as description,
	    dt.seat_count as seatCount,
	    dt.table_type as tableType,
	    dt.availability,
	    dt.occupied,
	    dt.createdOn,
	    dt.modifiedOn,
	    dt.hash_id as id,
	    dt.status,
	    res.name as restaurantName,
	    res.image as restaurantImage   
	    
	FROM dininng_table dt
	inner join restaurant res on res.id = dt.restaurant_id

	where dt.hash_id = ihash_id;
	
	else 
	
	set countFrom = IF(JSON_UNQUOTE(JSON_EXTRACT(idata,'$.from')) is null, 0, 
    JSON_UNQUOTE(JSON_EXTRACT(idata,'$.from')));
    set count = IF(JSON_UNQUOTE(JSON_EXTRACT(idata,'$.count')) is null, 20, 
   JSON_UNQUOTE( JSON_EXTRACT(idata,'$.count')));
    
	SELECT 
	    dt.table_code as tableCode,
	    dt.description as description,
	    dt.seat_count as seatCount,
	    dt.table_type as tableType,
	    dt.availability,
	    dt.occupied,
	    dt.createdOn,
	    dt.modifiedOn,
	    dt.hash_id as id,
	    dt.status,
	    res.name as restaurantName,
	    res.image as restaurantImage   
	    
	FROM dininng_table dt
	inner join restaurant res on res.id = dt.restaurent_id

	where dt.status = 1 and res.hash_id = ihash_id 
	
	order by dt.id 
	limit count,countFrom;
	
	end case;
END