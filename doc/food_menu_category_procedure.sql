CREATE DEFINER=`admin`@`%` PROCEDURE `food_menu_category_procedure`(in task text(120), in idata json)
BEGIN
	DECLARE ihash_id TEXT(426) DEFAULT null;
    declare countFrom int default 0;
    declare count int default 20;
    
    set ihash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.id'));
    
	case task	
	
	when 'insert' then 
		
		set @image_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.imageUrl'));
		set @thumb_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.thumbUrl'));
		set @description = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.description'));
		set @name = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.name'));
		set ihash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.restaurantId'));
		
		set @itemId = (SELECT SHA1(CONCAT(now(), rand(),
					(SELECT AUTO_INCREMENT 
					FROM information_schema.TABLES WHERE TABLE_NAME = "restaurant"),
					'restaurant')));
                 
		INSERT INTO food_menu_category
			(
			restaurant_id,
			name,
			description,
			image_url,
			thumb_url,
			createdOn,
			hash_id)
			VALUES
			(
			(Select id from restaurant where hash_id = ihash_id),
			@name,
			@description,
			@image_url,
			@thumb_url,
			now(),
			@itemId);
			
	when 'update' then 
		set @image_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.imageUrl'));
		set @thumb_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.thumbUrl'));
		set @description = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.description'));
		set @name = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.name'));
		
		
		update food_menu_category 
			set name = if(@name is null, name, @name) ,
			description = if(@description is null, description, @description) ,
			image_url = if(@image_url is null, image_url, @image_url) ,
			thumb_url = if(@thumb_url is null, thumb_url, @thumb_url) 
			
			where hash_id = ihash_id and id > 0;
	
	when 'delete' then
	
		update food_menu_category 
			set status = -1 
		where hash_id = ihash_id and id > 0;
	
	when 'get_by_id' then 
	
	SELECT    
	    fmc.name,
	    fmc.description,
	    fmc.image_url as imageUrl,
	    fmc.thumb_url as thumbUrl,
	    fmc.createdOn,
	    fmc.modifiedBy,
	    fmc.modifiedOn,
	    fmc.hash_id as id,
	    res.name as restaurantName,
	    res.image as restaurantImage   
	    
	FROM food_menu_category fmc
	inner join restaurant res on res.id = fmc.restaurant_id

	where fmc.hash_id = ihash_id;
	
	else 
	
	set countFrom = IF(JSON_EXTRACT(idata,'$.from') is null, 0, 
    JSON_EXTRACT(idata,'$.from'));
    set count = IF(JSON_EXTRACT(idata,'$.count') is null, 20, 
    JSON_EXTRACT(idata,'$.count'));
    
	SELECT 
	    fmc.name,
	    fmc.description,
	    fmc.image_url as imageUrl,
	    fmc.thumb_url as thumbUrl,
	    fmc.createdOn,
	    fmc.modifiedBy,
	    fmc.modifiedOn,
	    fmc.hash_id as id,
	    res.name as restaurantName,
	    res.image as restaurantImage   
	    
	FROM food_menu_category fmc
	inner join restaurant res on res.id = fmc.restaurant_id

	where fmc.status = 1 and res.hash_id = ihash_id 
	
	order by fmc.name 
	limit countFrom,count;
	
	end case;
END