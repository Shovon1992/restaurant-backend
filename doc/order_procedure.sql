CREATE DEFINER=`admin`@`%` PROCEDURE `order_procedure`(in task varchar(50), in iData json)
BEGIN
	declare ihash_id text(255) default null;
	set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.cartId') );
	set @cartId =  ( select id from cart where hash_id =  ihash_id);
   
    
    
    case task
    
		when 'createOrder' then 
			set @userId = (select userId from cart where id = @cartId);
			set @restaurantId = (select restaurantId from cart where id = @cartId);
			set @tableId = (select tableId from cart where id = @cartId);
			set @totalprice = JSON_EXTRACT(idata,'$.totalprice') ;
			set @listPrice = JSON_EXTRACT(idata,'$.listPrice') ;
			set @discount = JSON_EXTRACT(idata,'$.discount') ;
			set @CuponCode = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.CuponCode') );
			set @tax = JSON_EXTRACT(idata,'$.tax') ;
			set @transtionMethod = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.transtionMethod') );
			set @trnsactionId  = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.trnsactionId') );
					
            set @userMessage = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.userMessage') );
			
            if exists(select id from orders where cartId = @cartId ) then
				
                select "Cart Items already procced " as message, 300 as errorCode;
            else
				
	  	
	  	set @orderId = createHashId('orders');
			insert into orders (
				cartId,
				userId,
				restaurantId,
				tableId,
				userMessage,
				hash_id
			) values(
				@cartId,
				@userId,
				@restaurantId,
				@tableId,
				@userMessage,
				@orderId
				
			);
                
            set @lastId = (SELECT id FROM orders where hash_id = @orderId );
            
                insert into orders_items (
                			oredrId,
					foodItemId,
					priceId,
					addedQuantity,
                    hash_id
				) 			
			select @lastId, foodItemId, priceId, addedQuantity,  ( SELECT SHA1(CONCAT(now(),
            rand(),(SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = 'orders_items' ), "orders_items" )) ) 
            from cart_items where cartId = @cartId and status = 1
				;
				
			
					
			INSERT INTO orders_tracking
				(
				oredrId,
				oredrItemsId,
				message,
				propsedTime,
				actionByRestaurant,
				status,
				hash_id)
			select odItem.oredrId, odItem.id, 
            'processing', 30, od.restaurantId, 0,  ( SELECT SHA1(CONCAT(now(),
            rand(),(SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = 'orders_tracking' ), "orders_tracking" )) )
			from orders_items odItem
            left join orders od on od.id = odItem.oredrId 
			
            where odItem.oredrId = @lastId 
				;
		select @orderId as orderId;
			end if;
		update cart set status = -1 where id = @cartId and id > 0;
    # when resturant accept/ decline / processing state
    when 'add_order_tracking_status' then
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.orderItemId') );
			-- set @orderId = (select oredrId from orders_items where hash_id = ihash_id);
            set @oredrItemsId = (select id from orders_items where hash_id = ihash_id);
			set @message = JSON_EXTRACT(idata,'$.message');
			set @status = JSON_EXTRACT(idata,'$.status') ;
			set ihash_id = JSON_EXTRACT(idata,'$.actionByUser');
            if ihash_id then 
				set @actionByUser = (select id from user_info where hash_id = ihash_id);
            else 
				set @actionByUser = null; 
			end if;
            
			set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.actionByRestaurant') );
            if ihash_id != '' then 
				
				set @actionByRestaurant = (select id from restaurant where hash_id = ihash_id);
			else 
				set @actionByRestaurant = null;
            end if;    
            set @userMessage = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.userMessage') );
            set @propsedTime = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.propsedTime') );
            
            INSERT INTO orders_tracking
				(
				oredrItemsId,
				message,
				propsedTime,
                actionByUser,
				actionByRestaurant,
				status,
				hash_id
                )values (
                    @oredrItemsId,
                    @message,
                    @propsedTime,
                    @actionByUser,
                    @actionByRestaurant,
                    @status,
                    createHashId('orders_tracking')
                );
                
	when 'track_status_update' then 
	
		set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.actionByRestaurant'));
		set @actionByRestaurant = (Select id from restaurant where hash_id = ihash_id);
		
		set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.actionByUser'));
		set @actionByUser = (select id from user_info where  hash_id = ihash_id);
		
		set @message = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.message'));
		set @propsedTime = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.propsedTime'));
			
		set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.oredrItemsId') );
		set @status = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.status'));
		UPDATE orders_tracking
		SET

		message = IF(@message is null,message,@message),
		propsedTime = IF(@propsedTime is null, propsedTime,@propsedTime),
		actionByUser = IF(@actionByUser is null,actionByUser,@actionByUser),
		actionByRestaurant = IF(@actionByRestaurant is null,actionByRestaurant,@actionByRestaurant),
		status = @status
		WHERE oredrItemsId = ihash_id;
		
		
	when 'get_by_order_id' then 
	
		set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.orderId'));
		
		SELECT     
			res.hash_id as resturantId,
			dt.hash_id as tableId,
            oi.hash_id as itemId,
			od.userMessage as orderMessage,
			od.status,
			od.createdBy,
			od.createdOn,
			od.modifiedBy,
			od.modifiedOn,
			od.hash_id as orderId,
			fi.name as itemName,
			fi.description as itemDescription,
			If(otLatest.actionByUser is null,'restaurant','user') as trackMessageDoneBy,
			otLatest.status as trackStatus,
			otLatest.hash_id as trackId,
			otLatest.message as trackMessage,
			otLatest.propsedTime,
			fiImage.image_url as foodItemLargeImage,
			fiImage.image_thumb_url as foodItemThumbImage,
			ot.tracking,
			opayment.totalprice,
			opayment.listPrice,
			opayment.discount,
			opayment.CuponCode,
			opayment.tax,
			opayment.transtionMethod,
			opayment.trnsactionId,
			opayment.hash_id as paymentId,
			oi.addedQuantity,
			fprice.quantity as priceQuantity,
			fprice.price,
			fprice.currency as priceCurrency,
			fprice.unit as priceUnit
			
			FROM orders od 
			left join restaurant res on res.id = od.restaurantId
			left join dininng_table dt on dt.id = od.tableId
			left join user_info ui on ui.id = od.userId
			left join orders_items oi on oi.oredrId = od.id
			left join food_menu_item_price fprice on fprice.id = oi.priceId
			left join food_menu_item fi on fi.id = oi.foodItemId
			left join food_menu_item_image fiImage on fiImage.food_menu_item_id = fi.id and fiImage.set_as_logo = 1 
			left join orders_tracking otLatest on otLatest.id = (select id from orders_tracking where 
									oredrItemsId = oi.id order by id desc limit 1)
									
			left join orders_payment opayment on opayment.oredrId = od.id 
									
			left join (SELECT oredrItemsId, JSON_ARRAY(GROUP_CONCAT(JSON_OBJECT(
								'id', hash_id, 'message', message,	
								'propsedTime', propsedTime, 'actionByUser', IF(actionByUser is null,0,1),	
								'actionByRestaurant', IF(actionByRestaurant is null,0,1),
								'status', status
								) order by createdOn desc))  as tracking from orders_tracking 
								group by oredrItemsId)ot on ot.oredrItemsId = oi.id 
								
			where od.hash_id = ihash_id;
	
	when 'get_by_user_id' then 
	
		set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.userId'));
	
		SELECT     
		    res.hash_id as resturantId,
            res.name as restaurantName,
		    dt.table_code as tableId,
		    od.userMessage as orderMessage,
            
            (select count(*) from orders_items where oredrId = od.id ) as addedQuantity,
		    od.status,
		    od.createdBy,
		    od.createdOn,
		    od.modifiedBy,
		    od.modifiedOn,
		    od.hash_id as orderId 
		    #,fi.name as itemName,
		    #fi.description as itemDescription,
		    #If(ot.actionByUser is null,'restaurant','usser') as trackMessageDoneBy,
		    #ot.status as trackStatus,
		    #ot.hash_id as trackId,
		    #ot.message as trackMessage,
		    #ot.propsedTime,
		    #fiImage.image_url as foodItemLargeImage,
		    #fiImage.image_thumb_url as foodItemThumbImage,
		    #oi.addedQuantity,
			#fprice.quantity as priceQuantity,
			#fprice.price,
			#fprice.currency as priceCurrency,
			#fprice.unit as priceUnit
		    
		FROM orders od 
		left join restaurant res on res.id = od.restaurantId
		left join dininng_table dt on dt.id = od.tableId
		left join user_info ui on ui.id = od.userId
		#left join orders_items oi on oi.oredrId = od.id
		#left join food_menu_item_price fprice on fprice.id = oi.priceId
		#left join food_menu_item fi on fi.id = oi.foodItemId
		#left join food_menu_item_image fiImage on fiImage.food_menu_item_id = fi.id and fiImage.set_as_logo = 1 
		#left join orders_tracking ot on ot.id = (select id from orders_tracking where 
			#					oredrItemsId = oi.id order by id desc limit 1)
								
		where ui.hash_id = ihash_id;
	when 'get_by_restaurant_id' then 
	set ihash_id = JSON_UNQUOTE( JSON_EXTRACT(idata,'$.restaurantId'));
		set @totalOrder = (select count(*) from orders);
        set @totalUser = (select count(distinct userId) from orders);
		SELECT     
			@totalOrder as totalOrders,
            @totalUser as totalUsers,
		    res.hash_id as resturantId,
		    -- dt.hash_id as tableId,
            od.tableId as tableId,
		    od.userMessage as orderMessage,
		    od.status,
		    od.createdBy,
		    od.createdOn,
		    od.modifiedBy,
		    od.modifiedOn,
		    od.hash_id as orderId,
		    ui.name as userName,
		    dt.table_code as tableId,
            (select count(*) from orders_items where oredrId = od.id ) as totalItems
		FROM orders od 
		left join restaurant res on res.id = od.restaurantId
		left join dininng_table dt on dt.id = od.tableId
		left join user_info ui on ui.id = od.userId
		
								
		where res.hash_id = ihash_id;
	else 
		select * from orders;
    end case;
END