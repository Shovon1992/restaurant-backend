CREATE DEFINER=`admin`@`%` PROCEDURE `food_item_image_procedure`(in task text(120), in idata json)
BEGIN
	DECLARE ihash_id TEXT(426) DEFAULT null;
	
	set ihash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.id'));
			
	case task	
	
	when 'insert' then 
            	set ihash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.foodItemId'));
            
            	set @image_type = JSON_EXTRACT(idata,'$.imageType');
		
		set @image_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.iurl'));
		set @image_thumb_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.ithumbUrl'));
		
		set @itemId = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = "food_menu_item_image");
		set @itemHId = (SELECT SHA1(CONCAT(now(), rand(),@itemId,'food_menu_item_image')));
         
		
		INSERT INTO food_menu_item_image
			(
			food_menu_item_id,
			set_as_logo,
			image_url,
			image_thumb_url,
			image_type,
			hash_id)
			VALUES
			(
			(Select id from  food_menu_item where hash_id = ihash_id),
			0,
			@image_url,
			@image_thumb_url,
			@image_type,
			@itemHId);

		
			
		when 'delete' then
		
		update food_menu_item_image set status = -1 where hash_id = ihash_id and id > 0;
			
		else
		SELECT 
		    fmiPrice.image_url,
		    fmiPrice.image_thumb_url,
		    fmiPrice.image_type,
		    fmiPrice.set_as_logo,
		
		    fmiPrice.modifiedOn,
		    fmiPrice.hash_id as id 
		FROM food_menu_item_image fmiPrice 
		left join food_menu_item fmi on fmi.id = fmiPrice.food_menu_item_id 
		where fmi.hash_id = ihash_id and fmiPrice.status = 1;
		
		end case;
END