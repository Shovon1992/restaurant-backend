CREATE DEFINER=`admin`@`%` PROCEDURE `restaurant_procedure`(
	in task text(120), /* select insert update */
	in accountId text(456), /* User account id who is creating res. */
	in idata json, /* Body data for operation*/
	inout itemId text(456) /* Restaurand id */
)
BEGIN
	
		set @tagline = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.tagLine'));
		set @name = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.name'));
		set @email = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.email'));
		set @phone = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.phone'));
		set @image = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.image'));
        set @reg_no = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.reg_no'));   
        
	case task   /* For get all restuurant create new resturant and update resturant details */ 
		when 'insert' then
			set @resId  = (SELECT AUTO_INCREMENT 
					FROM information_schema.TABLES WHERE TABLE_NAME = "restaurant");
                    
            set itemId = (SELECT SHA1(CONCAT(now(),@resId,'restaurant')));
			
			INSERT INTO restaurant (
					tagline,
					name,
					email,
					phone,
					image,
					createdBy,
					createdOn,
					hash_id,
					reg_no
				)
				VALUES(
					@tagline,
					@name,
					@email,
					@phone,
					@image,
					(select id from user_info where hash_id = accountId), /* getting user id from user Table*/
					now(),
					itemId,
                    @reg_no
				);
                set @authId = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.authId'));
                
                if @authId is not null then 
					INSERT INTO restaurant_cognito_map
						(
						restaurant_id,
						cognito_id)
						VALUES
						(
						@resId,
						@authId);
				end if;
                    
		when 'update' then
			UPDATE restaurant
				SET
				tagline = @tagline,
				name = @name,
				email = @email,
				phone = @phone,
				image = @image,
                reg_no = @reg_no,
				modifiedBy = (select id from user_info where hash_id = accountId),
				modifiedOn = now()
				WHERE hash_id = itemId;
		else
			SELECT 
				res.tagline,
				res.name,
				res.email,
				res.phone,
				res.image,
                res.reg_no,
				res.hash_id as id
			
		FROM restaurant res;
    end case;
END