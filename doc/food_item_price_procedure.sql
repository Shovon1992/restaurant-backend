CREATE DEFINER=`admin`@`%` PROCEDURE `food_item_price_procedure`(in task text(120), in idata json)
BEGIN
	DECLARE ihash_id TEXT(426) DEFAULT null;
	
	set ihash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.id'));
			
	case task	
	
	when 'insert' then 
            	set ihash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.foodItemId'));
            
            	set @quantity = JSON_EXTRACT(idata,'$.quantity');
		set @price = JSON_EXTRACT(idata,'$.price');
		set @currency = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.currency'));
		set @unit = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.unit'));
		
		set @itemId = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = "food_menu_item_price");
		set @itemHId = (SELECT SHA1(CONCAT(now(), rand(),@itemId,'food_menu_item_price')));
         
		INSERT INTO food_menu_item_price
		(
		food_menu_item_id,
		quantity,
		price,
		currency,
		unit,
		hash_id)
		VALUES
		(
		(Select id from  food_menu_item where hash_id = ihash_id),
		@quantity,
		@price,
		@currency,
		@unit,
		@itemHId );
		
		when 'update' then
		set @quantity = JSON_EXTRACT(idata,'$.quantity');
		set @price = JSON_EXTRACT(idata,'$.price');
		
			update food_menu_item_price set 
			
				quantity = @quantity,
				price =@price,
				modifiedOn = now()
				
				where food_menu_item_id = (select id from  food_menu_item where hash_id = ihash_id);
			
		when 'delete' then
		
		update food_menu_item_price set status = -1 where hash_id = ihash_id and id > 0;
			
		else
		SELECT 
		    fmiPrice.quantity,
		    fmiPrice.price,
		    fmiPrice.currency,
		    fmiPrice.unit,
		    fmiPrice.status,
		    fmiPrice.modifiedBy,
		    fmiPrice.modifiedOn,
		    fmiPrice.hash_id as id 
		FROM food_menu_item_price fmiPrice 
		left join food_menu_item fmi on fmi.id = fmiPrice.food_menu_item_id 
		where fmi.hash_id = ihash_id and fmiPrice.status = 1;
		
		end case;
END