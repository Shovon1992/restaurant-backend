CREATE DEFINER=`admin`@`%` FUNCTION `getIdFromHash`(tableName varchar(50),
    hashId text
) RETURNS int(11)
BEGIN
    return utility_procedure('getIdFromHash', '{ "tableName: "'+tableName +'","hashId " :"'+hashId+'" }');
END