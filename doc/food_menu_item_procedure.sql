CREATE DEFINER=`admin`@`%` PROCEDURE `food_menu_item_procedure`(in task text(120), in idata json)
BEGIN
	DECLARE ihash_id TEXT(426) DEFAULT null;
    declare countFrom int default 0;
    declare count int default 20;
    
    set ihash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.id'));
    
	case task	
	
	when 'insert' then 
		
		set @image_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.imageUrl'));
		set @thumb_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.thumbUrl'));
		set @description = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.description'));
		set @name = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.name'));
		set @taste = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.taste'));
		set @ingrediant = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.ingrediant'));
		
		set @veg = JSON_EXTRACT(idata,'$.veg');
		set @availability_status = JSON_EXTRACT(idata,'$.availabilityStatus');
		
		set @image_type = JSON_EXTRACT(idata,'$.imageType');
		
		set ihash_id = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.foodCategoryId'));
		
		set @itemId = (SELECT AUTO_INCREMENT 
					FROM information_schema.TABLES WHERE TABLE_NAME = "food_menu_item");
					
		set @itemIHd = (SELECT SHA1(CONCAT(now(), rand(),@itemId,'food_menu_item')));
                 
		INSERT INTO food_menu_item
				(
				name,
				description,
				food_menu_category_id,
				taste,
				veg,
				ingrediant,
				availability_status,
				hash_id)
				VALUES
				(
				@name,
				@description,
				(Select id from food_menu_category where hash_id = ihash_id),
				@taste,
				@veg,
				@ingrediant,
				@availability_status,
				@itemIHd);
				
		INSERT INTO food_menu_item_image
				(
				food_menu_item_id,
				set_as_logo,
				image_url,
				image_thumb_url,
				image_type,
				hash_id)
				VALUES
				(
				@itemId,
				1,
				@image_url,
				@thumb_url,
				@image_type,
				(SELECT SHA1(CONCAT(now(), rand(),(SELECT AUTO_INCREMENT 
					FROM information_schema.TABLES WHERE TABLE_NAME = 
                    "food_menu_item_image"),'food_menu_item_image')))
                    );
						
			
	when 'update' then 
		set @image_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.imageUrl'));
		set @thumb_url = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.thumbUrl'));
		set @description = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.description'));
		set @name = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.name'));
		set @taste = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.taste'));
		set @ingrediant = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.ingrediant'));
		
		set @veg = JSON_EXTRACT(idata,'$.veg');
		set @availability_status = JSON_EXTRACT(idata,'$.availabilityStatus');
		
		set @image_type = JSON_EXTRACT(idata,'$.imageType');
		
		update food_menu_item 
			set name = if(@name is null, name, @name) ,
			description = if(@description is null, description, @description) ,
			taste = if(@taste is null, taste, @taste) ,
			ingrediant = if(@ingrediant is null, ingrediant, @ingrediant), 
			veg = if(@veg is null, veg, @veg) ,
			availability_status = if(@availability_status is null, availability_status, @availability_status) 
			
			where hash_id = ihash_id and id > 0;
			
		update food_menu_item_image 
			set 
				image_url = if(@image_url is null, image_url, @image_url) ,
				image_thumb_url = if(@image_thumb_url is null, image_thumb_url, @image_thumb_url) ,
				image_type = if(@image_type is null, image_type, @image_type) 
				
			where food_menu_item_id = (select id from food_menu_item where hash_id = ihash_id) and id > 0 and set_as_logo = 1;
				
	
	when 'delete' then
	
		update food_menu_item 
			set status = -1 
		where hash_id = ihash_id and id > 0;
	
	when 'get_by_id' then 
	
	SELECT 
	    fmc.name as categoryName,
	    fmc.description as categoryDescription,
	    fmc.image_url as categoryImageUrl,
	    fmc.thumb_url as categoryThumbUrl,
	    fmc.hash_id as categoryId,
	    res.name as restaurantName,
	    res.image as restaurantImage,
	    fmi.name,
	    fmi.description,
	    fmi.taste,
	    fmi.veg,
	    fmi.ingrediant,
	    fmi.availability_status as availabilityStatus,
	    fmi.status,
	    fmi.modifiedOn,
	    fmiImage.image_url as imageUrl,
	    fmiImage.image_thumb_url as thumbUrl,
	    fmiImage.image_type as imageType,
        fmiPrice.hash_id as priceId,
	    fmiPrice.quantity,
	    fmiPrice.price,
	    fmiPrice.currency,
	    fmiPrice.unit,
	    fmiImageS.fItemImages,
	    fmiPriceS.fItemPrices,	    
	    fmi.hash_id as id   
	    
	FROM food_menu_item fmi 
	inner join food_menu_category fmc on fmc.id = fmi.food_menu_category_id
	inner join restaurant res on res.id = fmc.restaurant_id
	inner join food_menu_item_image fmiImage on fmiImage.food_menu_item_id =fmi.id and fmi.set_as_logo = 1 
	inner join food_menu_item_price fmiPrice on fmiPrice.id = (select id from food_menu_item_price 
									where food_menu_item_id =fmi.id and status=1
									order by price limit 1)
									
	inner join (Select food_menu_item_id, json_array(group_concat(json_object(
					'imageUrl',image_url,'thumbUrl',image_thumb_url,
					'set_as_logo',set_as_logo,'imageType',image_type))) as fItemImages
			 from food_menu_item_image where status = 1 group by food_menu_item_id) fmiImageS 
			 on fmiImageS.food_menu_item_id =fmi.id 
			 
 	inner join (Select food_menu_item_id,  json_array(group_concat(json_object('quantity',quantity,'price', price, 'currency',currency,
 	 'unit',unit))) as fItemPrices  
 			from food_menu_item_price  where status = 1 group by food_menu_item_id) fmiPriceS 
 			 on fmiPriceS.food_menu_item_id =fmi.id 

	where fmi.status = 1 and fmc.hash_id = ihash_id 
	
	order by fmc.name 
	;
	
	when 'food_search_by_res_id' then 
		set countFrom = IF(JSON_UNQUOTE(JSON_EXTRACT(idata,'$.from')) is null, 0, 
    		JSON_UNQUOTE(JSON_EXTRACT(idata,'$.from')));
    
    	set count = IF(JSON_UNQUOTE(JSON_EXTRACT(idata,'$.count')) is null, 20, 
    		JSON_UNQUOTE(JSON_EXTRACT(idata,'$.count')));
	set @fname = JSON_UNQUOTE(JSON_EXTRACT(idata,'$.name'));
    
	SELECT 
	    fmc.name as categoryName,
	    fmc.description as categoryDescription,
	    fmc.image_url as categoryImageUrl,
	    fmc.thumb_url as categoryThumbUrl,
	    fmc.hash_id as categoryId,
	    res.name as restaurantName,
	    res.image as restaurantImage,
	    fmi.name,
	    fmi.description,
	    fmi.taste,
	    fmi.veg,
	    fmi.ingrediant,
	    fmi.availability_status as availabilityStatus,
	    fmi.status,
	    fmi.modifiedOn,
	    fmiImage.image_url as imageUrl,
	    fmiImage.image_thumb_url as thumbUrl,
	    fmiImage.image_type as imageType,
	    fmiPrice.quantity,
	    fmiPrice.price,
        fmiPrice.hash_id as priceId,
	    fmiPrice.currency,
	    fmiPrice.unit,
	    fmi.hash_id as id   
	    
	FROM food_menu_item fmi 
	left join food_menu_category fmc on fmc.id = fmi.food_menu_category_id
	left join restaurant res on res.id = fmc.restaurant_id
	left join food_menu_item_image fmiImage on fmiImage.food_menu_item_id =fmi.id and fmiImage.set_as_logo = 1 
	left join food_menu_item_price fmiPrice on fmiPrice.id = (select id from food_menu_item_price 
									where food_menu_item_id =fmi.id and status=1
									order by price limit 1)

	where fmi.status = 1 and fmi.name like concat('%',@fname,'%') and res.hash_id = ihash_id 
	
	order by fmi.name 
	limit countFrom, count;
	
	else 
	
	set countFrom = IF(JSON_UNQUOTE(JSON_EXTRACT(idata,'$.from')) is null, 0, 
    		JSON_UNQUOTE(JSON_EXTRACT(idata,'$.from')));
    
    	set count = IF(JSON_UNQUOTE(JSON_EXTRACT(idata,'$.count')) is null, 20, 
    		JSON_UNQUOTE(JSON_EXTRACT(idata,'$.count')));
    
	SELECT 
	    fmc.name as categoryName,
	    fmc.description as categoryDescription,
	    fmc.image_url as categoryImageUrl,
	    fmc.thumb_url as categoryThumbUrl,
	    fmc.hash_id as categoryId,
	    res.name as restaurantName,
	    res.image as restaurantImage,
	    fmi.name,
	    fmi.description,
	    fmi.taste,
	    fmi.veg,
	    fmi.ingrediant,
	    fmi.availability_status as availabilityStatus,
	    fmi.status,
	    fmi.modifiedOn,
	    fmiImage.image_url as imageUrl,
	    fmiImage.image_thumb_url as thumbUrl,
	    fmiImage.image_type as imageType,
	    fmiPrice.quantity,
        fmiPrice.hash_id as priceId,
	    fmiPrice.price,
	    fmiPrice.currency,
	    fmiPrice.unit,
	    fmi.hash_id as id   
	    
	FROM food_menu_item fmi 
	left join food_menu_category fmc on fmc.id = fmi.food_menu_category_id
	left join restaurant res on res.id = fmc.restaurant_id
	left join food_menu_item_image fmiImage on fmiImage.food_menu_item_id =fmi.id and fmiImage.set_as_logo = 1 
	left join food_menu_item_price fmiPrice on fmiPrice.id = (select id from food_menu_item_price 
									where food_menu_item_id =fmi.id and status=1
									order by price limit 1)

	where fmi.status = 1 and fmc.hash_id = ihash_id 
	
	order by fmi.name 
	limit countFrom, count;
	
	end case;
END