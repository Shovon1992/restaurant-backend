// dependencies
var async = require('async');
var AWS = require('aws-sdk');
//var gm = require('gm').subClass({ imageMagick: true }); // Enable ImageMagick integration.
var util = require('util');
var sharp = require('sharp');

// constants
var MAX_WIDTH  = 240;
var MAX_HEIGHT = 240;

const DESTI_FOOD_CATEGORY_PIX_THUMB = "menume-dev-food-category-thumb-doc";
const SRC_FOOD_CATEGORY_PIX_THUMB = "menume-dev-food-category-doc";

const DESTI_FOOD_ITEM_PIX_THUMB = "menume-dev-food-item-thumb-doc";
const SRC_FOOD_ITEM_PIX_THUMB = "menume-dev-food-item-doc";

const DESTI_RESTAURANT_PIX_THUMB = "menume-dev-restaurant-thumb-doc";
const SRC_RESTAURANT_PIX_THUMB = "menume-dev-restaurant-doc";


// get reference to S3 client 
var s3 = new AWS.S3();
 
exports.handler = function(event, context, callback) {
    // Read options from the event.
    console.log("Reading options from event:\n", util.inspect(event, {depth: 5}));
    var srcBucket = event.Records[0].s3.bucket.name;
    // Object key may have spaces or unicode non-ASCII characters.

    var srcKey    =
    decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " "));  
    var dstBucket = DESTI_FOOD_CATEGORY_PIX_THUMB ;
    var dstKey    =  srcKey;
    console.log("srcKey.match('image')",srcKey.match('image'));
    if(srcKey.match('image'))
    {
    switch(srcBucket) {
        case SRC_FOOD_CATEGORY_PIX_THUMB:
                dstBucket = DESTI_FOOD_CATEGORY_PIX_THUMB;
                MAX_HEIGHT = 120;
                MAX_WIDTH = 120;
        // code block
        break;
       
        case SRC_FOOD_ITEM_PIX_THUMB:
            dstBucket = DESTI_FOOD_ITEM_PIX_THUMB;
        break;
        
        case SRC_RESTAURANT_PIX_THUMB:
            dstBucket = DESTI_RESTAURANT_PIX_THUMB;
        break;
        
        default:
        // code block
         break;
    }
}else{
    return;
}
  
    
console.log("desti bucket",dstBucket);
    // Sanity check: validate that source and destination are different buckets.
    if (srcBucket == dstBucket) {
        callback("Source and destination buckets are the same.");
        return;
    }

    // Infer the image type.
    var typeMatch = srcKey.match(/\.([^.]*)$/);
    if (!typeMatch) {
        callback("Could not determine the image type.");
        return;
    }
    var imageType = typeMatch[1].toLowerCase();
    if (imageType != "jpg" && imageType != "png" && imageType != "jpeg") {
        callback(`Unsupported image type: ${imageType}`);
        return;
    }

    // Download the image from S3, transform, and upload to a different S3 bucket.
    async.waterfall([
        function download(next) {
            // Download the image from S3 into a buffer.
            console.log("source bucket",srcBucket)
            console.log("source key",srcKey)
            
            s3.getObject({
                    Bucket: srcBucket,
                    Key: srcKey
                },
                next);
            },
        function transform(response, next) {
            /* gm(response.Body).size(function(err, size) {
                // Infer the scaling factor to avoid stretching the image unnaturally.
                var scalingFactor = Math.min(
                    MAX_WIDTH / size.width,
                    MAX_HEIGHT / size.height
                );
                var width  = scalingFactor * size.width;
                var height = scalingFactor * size.height;

                // Transform the image buffer in memory.
                this.resize(width, height)
                    .toBuffer(imageType, function(err, buffer) {
                        if (err) {
                            next(err);
                        } else {
                            next(null, response.ContentType, buffer);
                        }
                    });
            
            }); */

            // Transform the image buffer in memory.
            sharp(response.Body)
            .resize(MAX_WIDTH, MAX_HEIGHT)
            .withMetadata()
            .toBuffer(imageType, function(err, buffer) {
                    if (err) {
                        next(err);
                    } else {
                        next(null, response.ContentType, buffer);
                    }
                });
        },
        function upload(contentType, data, next) {
            // Stream the transformed image to a different S3 bucket.
            console.log("desti bucket",dstBucket)
            console.log("desti key",dstKey)
            //console.log("desti data",data)
            s3.putObject({
                    Bucket: dstBucket,
                    Key: dstKey,
                    Body: data,
                    ContentType: contentType
                },
                next);
            }
        ], function (err) {
            if (err) {
                console.error(
                    'Unable to resize ' + srcBucket + '/' + srcKey +
                    ' and upload to ' + dstBucket + '/' + dstKey +
                    ' due to an error: ' + err
                );
            } else {
                console.log(
                    'Successfully resized ' + srcBucket + '/' + srcKey +
                    ' and uploaded to ' + dstBucket + '/' + dstKey
                );
            }

            callback(null, "message");
        }
    );
};
