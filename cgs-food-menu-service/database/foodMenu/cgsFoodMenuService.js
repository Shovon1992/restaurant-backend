let udQueries = require('./quaries/statement');
var excute = require('../mariaDbCon/exe');

module.exports = {
    createFoodMenucategory: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodCategory;
       excute.execute(sql,{'task': 'insert','data': data} , callBack);
        
    },

    updateFoodMenucategory: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodCategory;
       excute.execute(sql,{'task': 'update','data': data} , callBack);
        
    },

    getFoodMenucategoryById: function(id, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodCategory;
       excute.execute(sql,{'task': 'get_by_id','data': JSON.stringify({'id':id})} , callBack);
        
    },
    getFoodMenucategoryByRestaurentId: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodCategory;
        var idata = {id:data.restaurantId, count:data.count, from:data.from};
       excute.execute(sql,{'task': 'default', 'data': JSON.stringify(idata)} , callBack);
        
    },
    deleteFoodMenucategory: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodCategory;
       excute.execute(sql,{'task': 'delete','data': data} , callBack);
        
    },

    createFoodMenuItem: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItem;
       excute.execute(sql,{'task': 'insert','data': data} , callBack);
        
    },

    updateFoodMenuItem: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItem;
        excute.execute(sql,{'task': 'update','data': data} , callBack);
        
    },


    deleteFoodMenuItem: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItem;
        excute.execute(sql,{'task': 'delete','data': data} , callBack);
        
    },
    getFoodMenuItemByCategoryId: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItem;
        var idata = {id:data.categoryId, count:data.count, from:data.from};
       excute.execute(sql,{'task': 'get_by_category_id','data': JSON.stringify(idata)} , callBack);
        
    },
    searchFoodMenuItemByRestaurantId: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItem;
        var idata = {id:data.restaurantId,name:data.foodname, count:data.count, from:data.from};
       excute.execute(sql,{'task': 'food_search_by_res_id','data': JSON.stringify(idata)} , callBack);
        
    },
    getFoodMenuItemById: function(id, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItem;
       excute.execute(sql,{'task': 'get_by_id','data': JSON.stringify({'id':id})} , callBack);
        
    },

    getFoodMenuItemByRestaurant: function(id, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItem;
       excute.execute(sql,{'task': 'get_by_restaurant_id','data': JSON.stringify({'id':id})} , callBack);
        
    },

    createFoodMenuItemPrice: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItemPrice;
       excute.execute(sql,{'task': 'insert','data': data} , callBack);
        
    },
    deleteFoodMenuItemPrice: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItemPrice;
       excute.execute(sql,{'task': 'delete','data': data} , callBack);
        
    },
    getFoodMenuItemPrice: function(id, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItemPrice;
       excute.execute(sql,{'task': 'get','id': id} , callBack);
        
    },

    createFoodMenuItemImage: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItemImage;
       excute.execute(sql,{'task': 'insert','data': data} , callBack);
        
    },
    deleteFoodMenuItemImage: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItemImage;
       excute.execute(sql,{'task': 'delete','data': data} , callBack);
        
    },
    getFoodMenuItemImage: function(id, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodItemImage;
       excute.execute(sql,{'task': 'get','id': id} , callBack);
        
    },
} 