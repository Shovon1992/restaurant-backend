'use strict';
const dbService = require('./database/cgsFoodMenuDbService');

module.exports.foodMenuCategory = (event, context, callback) => {
  switch(event.httpMethod) {
    case 'PUT' : 
      dbService.foodMenuDbService.updateFoodMenucategory(event.body,callback);
      break;
    case 'GET' : 
    const pathParameter =  event.pathParameters;
    if(pathParameter.id != null){
      dbService.foodMenuDbService.getFoodMenucategoryById(pathParameter.id, callback);
    }else{
      dbService.foodMenuDbService.getFoodMenucategoryByRestaurentId(pathParameter, callback);
    }
      break;

    case 'POST' : 
    dbService.foodMenuDbService.createFoodMenucategory(event.body, callback);
    break;

    case 'PATCH' : 
    dbService.foodMenuDbService.deleteFoodMenucategory(event.body, callback);
    break;
  }
}

module.exports.foodMenuItem = (event, context, callback) => {
  switch(event.httpMethod) {
    case 'PUT' : 
      dbService.foodMenuDbService.updateFoodMenuItem(event.body,callback);
      break;
    case 'GET' : 
    const pathParameter =  event.pathParameters;
    if(pathParameter.id != null){
      dbService.foodMenuDbService.getFoodMenuItemById(pathParameter.id, callback);
    }else{
      dbService.foodMenuDbService.getFoodMenuItemByCategoryId(pathParameter, callback);
    }
      break;

    case 'POST' : 
    dbService.foodMenuDbService.createFoodMenuItem(event.body, callback);
    break;

    case 'PATCH' : 
    dbService.foodMenuDbService.deleteFoodMenuItem(event.body, callback);
    break;
  }
}

module.exports.foodMenuItemPrice = (event, context, callback) => {
  switch(event.httpMethod) {
    /* case 'PUT' : 
      dbService.foodMenuDbService.updateFoodMenuItem(event.body,callback);
      break; */
    case 'GET' : 
    const pathParameter =  event.pathParameters;
    
    dbService.foodMenuDbService.getFoodMenuItemPrice(pathParameter.foodItemId, callback);
    
      break;

    case 'POST' : 
    dbService.foodMenuDbService.createFoodMenuItemPrice(event.body, callback);
    break;

    case 'PATCH' : 
    dbService.foodMenuDbService.deleteFoodMenuItemPrice(event.body, callback);
    break;
  }
}


module.exports.foodMenuItemImage = (event, context, callback) => {
  switch(event.httpMethod) {
    /* case 'PUT' : 
      dbService.foodMenuDbService.updateFoodMenuItem(event.body,callback);
      break; */
    case 'GET' : 
    const pathParameter =  event.pathParameters;
    
    dbService.foodMenuDbService.getFoodMenuItemImage(pathParameter.foodItemId, callback);
    
      break;

    case 'POST' : 
    dbService.foodMenuDbService.createFoodMenuItemImage(event.body, callback);
    break;

    case 'PATCH' : 
    dbService.foodMenuDbService.deleteFoodMenuItemImage(event.body, callback);
    break;
  }
}

module.exports.foodSearch = (event, context, callback) => {
  switch(event.httpMethod) {
    /* case 'PUT' : 
      dbService.foodMenuDbService.updateFoodMenuItem(event.body,callback);
      break; */
    case 'GET' : 
    const pathParameter =  event.pathParameters;
    
    dbService.foodMenuDbService.searchFoodMenuItemByRestaurantId(pathParameter, callback);
    
      break;

  }
}