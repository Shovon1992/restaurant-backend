// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access Cloud Firestore.
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
 exports.sendNotification = functions.https.onRequest((request, response) => {
   functions.logger.info("Hello logs!", {structuredData: true});
   functions.logger.info("Hello logs! request: ", request);
   if(request.method !== "POST"){
    res.status(400).send('Please send a POST request');
    return;
   }
   let data = request.body;
   // This registration token comes from the client FCM SDKs.
    var registrationToken = data.registrationToken;

    var message = {
    data: {
        title: data.header,        
        body: data.message,
        notificationId: data.notificationId
       },
    token: registrationToken
    };

    // Send a message to the device corresponding to the provided
    // registration token.
    admin.messaging().send(message)
    .then((response) => {
    // Response is a message ID string.
    console.log('Successfully sent message:', response);
    response.send("Successfully sent message:");
    })
    .catch((error) => {
    console.log('Error sending message:', error);
    response.send("Error sending message.");
    });

   
 });
