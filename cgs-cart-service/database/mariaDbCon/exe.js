var con = require('./connection_pool');


const responseSuccess = {
    statusCode: 200,
    headers: {
        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
    },
    body: JSON.stringify({
        message: 'function executed successfully!'
    }),
};

const responseError = {
    statusCode: 400,
    headers: {
        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
    },
    body: JSON.stringify({
        message: 'no db connection'
    }),
};

exports.execute = function(sql,data,callback){
    console.log(sql);
    console.log(data);
    var pool = con.establishConnection();
    pool.getConnection((error, connection) =>{
            if (error) throw error;
              
              
    connection.query(sql,data,(error, results, fields)=> {
        //--------------connection close
        connection.release();
        connection.destroy();
        //----------------------------------
        //callback(error, results)
        if (error) {
            console.log('error',error, results)
            responseError.body = JSON.stringify({
                message : 'Database error', 
                error: error,
                messageCode : 500
            });
            callback(null,responseError);
        }
        // connected!
        else {
            //console.log('success',error, results, callback(null, responseSuccess));
            responseSuccess.body = JSON.stringify({
                data : results,
                message: 'Success',
                messageCode : 200,
                statusCode: 200
            });
            callback(null, responseSuccess);
            
            if (error) throw error;
            
        }
        
        });
        
        
    });
}


/*
exports.execute =  function(sql, data, callback) {
     connection.connect(function(err) {
        if (err) {
          console.error('error connecting: ' + err.stack);
          return;
        }
       
        console.log('connected as id ' + connection.threadId );
        connection.query(sql, function (error, results, fields) {
            
            if (error) {
                responseError.body = JSON.stringify({
                    message : 'Database error', 
                    error: error,
                    messageCode : 500
                });
                callback(null,responseError);
            }
            // connected!
            else {
                responseSuccess.body = JSON.stringify({
                    data : results,
                    message: 'Success',
                    messageCode : 200
                });
                callback(null, responseSuccess);
            }
            connection.destroy();
          });
          
      });


}; */

