let udQueries = require('./quaries/statement');
var excute = require('../mariaDbCon/exe');

module.exports = {
    createFoodMenucart: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodCart;
       excute.execute(sql,{'task': 'createCart','data': data} , callBack);
        
    },

    updateFoodcartQuantity: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodCart;
       excute.execute(sql,{'task': 'update','data': data} , callBack);
        
    },

    getFoodCartDetailByRestaurentId: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodCart;
        var idata = {userId:data.userId, restaurantId:data.restaurantId, tableId:data.tableId};
       excute.execute(sql,{'task': 'getCartDetails', 'data': JSON.stringify(idata)} , callBack);
        
    },
    deleteFromcart: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodCart;
       excute.execute(sql,{'task': 'deleteFromCart','data': data} , callBack);
        
    },

    //---------------------------------------order------------------
    createFoodMenuOrder: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodMenuOrder;
       excute.execute(sql,{'task': 'createOrder','data': data} , callBack);
        
    },

    updateFoodTackOrder: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodMenuOrder;
       excute.execute(sql,{'task': 'track_status_update','data': data} , callBack);
        
    },

    getFoodOrderByUserId: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodMenuOrder;
        var idata = {userId:data.userId};
       excute.execute(sql,{'task': 'get_by_user_id', 'data': JSON.stringify(idata)} , callBack);
        
    },
    getFoodOrderByType: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodMenuOrder;
        var procedure = (data.type == 'user' ) ? 'get_by_user_id' : 'get_by_restaurant_id';
        var idata = (data.type == 'user' ) ? {userId: data.id} : {restaurantId: data.id};
        excute.execute(sql,{'task': procedure, 'data': JSON.stringify(idata)} , callBack);
        
    },
    /**Add oder tracking status */
    addOrderTarcking: function (data,callBack){
        var sql = udQueries.foodMenuOrder;
        console.log(JSON.stringify(data));
        excute.execute(sql,{'task': 'add_order_tracking_status', 'data': JSON.stringify(data)} , callBack);
    },
    getFoodOrderById: function(data, callBack){
        //data = JSON.parse(data);
        var sql = udQueries.foodMenuOrder;
        var idata = {orderId:data.orderId};
       excute.execute(sql,{'task': 'get_by_order_id', 'data': JSON.stringify(idata)} , callBack);
        
    },
} 