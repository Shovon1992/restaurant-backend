'use strict';
const dbService = require('./database/cgsFoodMenuCartDbService');

module.exports.foodMenuCart = (event, context, callback) => {
  switch(event.httpMethod) {
    case 'PUT' : 
      dbService.foodCartDbService.updateFoodcartQuantity(event.body,callback);
      break;
    case 'GET' : 
    const pathParameter =  event.pathParameters;
    dbService.foodCartDbService.getFoodCartDetailByRestaurentId(pathParameter, callback);
      break;

    case 'POST' : 
    dbService.foodCartDbService.createFoodMenucart(event.body, callback);
    break;

    case 'PATCH' : 
    dbService.foodCartDbService.deleteFromcart(event.body, callback);
    break;
  }
}


module.exports.foodMenuOrder = (event, context, callback) => {
  switch(event.httpMethod) {
    case 'PUT' : 
      dbService.foodCartDbService.updateFoodTackOrder(event.body,callback);
      break;
    case 'GET' : 
    const pathParameter =  event.pathParameters;
    if(pathParameter.orderId != null)
    {
      dbService.foodCartDbService.getFoodOrderById(pathParameter, callback);
    }else{
      dbService.foodCartDbService.getFoodOrderByUserId(pathParameter, callback);
    }
    
      break;

    case 'POST' : 
    dbService.foodCartDbService.createFoodMenuOrder(event.body, callback);
    break;


  }
}


/**
 * for getting order details by resturant id
 */

 module.exports.orderDetails = (event,context,callback) =>{
  switch(event.httpMethod) {
    case 'GET' : 
    const pathParameter =  event.pathParameters;
    if(pathParameter.type)
    {
      dbService.foodCartDbService.getFoodOrderByType(pathParameter, callback);
    }
  }
 }

 /**
  * For update tracking details
  */

 module.exports.orderTracking = (event, context, callback) =>{
  switch(event.httpMethod) {
    case 'POST' : 
      dbService.foodCartDbService.addOrderTarcking(event.body, callback);
      // const pathParameter =  event.pathParameters;
      // if(pathParameter.type)
      // {
      //   dbService.foodCartDbService.addOrderTarcking(event.body, callback);
      // }
  }
 }